# talley-rs

[![Current](https://img.shields.io/crates/v/tally-rs.svg)](https://crates.io/crates/tally-rs)
[![Documentation](https://docs.rs/tally-rs/badge.svg)](https://docs.rs/tally-rs)

GTK-based tally taking application for manually tracking and counting custom
occurrences.

Application de comptage basée sur GTK pour suivre et compter manuellement les
occurrences personnalisées.

ciocaXmT4hwxDrvUS8AskZxGW34FAmjku4h

https://codeberg.org/api/packages/h759bkyo4/cargo/