use std::cell::Cell;

use gtk::{glib, prelude::*, subclass::prelude::*};

#[derive(Default, gtk::CompositeTemplate, glib::Properties)]
#[template(file = "window.ui")]
#[properties(wrapper_type = super::TallyWindow)]
pub struct TallyWindow {
    #[property(get, set)]
    counter: Cell<i32>,
    #[property(get, set)]
    lock_state: Cell<bool>,
    #[template_child]
    pub count_label: TemplateChild<gtk::Label>,
    #[template_child]
    pub plus: TemplateChild<gtk::Button>,
    #[template_child]
    pub lock: TemplateChild<gtk::Button>,
    // #[template_child]
    // pub text: TemplateChild<gtk::Entry>,
    // #[template_child]
    // pub add_entry: TemplateChild<gtk::Button>,
}

#[glib::object_subclass]
impl ObjectSubclass for TallyWindow {
    const NAME: &'static str = "TallyWindow";
    type Type = super::TallyWindow;
    type ParentType = gtk::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
        klass.bind_template_instance_callbacks();
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

#[glib::derived_properties]
impl ObjectImpl for TallyWindow {}
impl WidgetImpl for TallyWindow {}
impl WindowImpl for TallyWindow {}
impl ApplicationWindowImpl for TallyWindow {}
