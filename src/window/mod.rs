// Since Gtk4 v4.10 gtk4::Dialog is deprecated and gtk4-rs's examples minimum
// version is v4.10
#[allow(deprecated)]
mod imp;

#[allow(unused_imports)]
use gtk::{glib, prelude::*, subclass::prelude::*};

glib::wrapper! {
    pub struct TallyWindow(ObjectSubclass<imp::TallyWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow;
}

#[gtk::template_callbacks]
impl TallyWindow {
    pub fn new<P: IsA<gtk::Application>>(app: &P) -> Self {
        let obj = glib::Object::builder().property("application", app);
        obj.build()
    }

    /// Callback handler for gtk::Button plus.
    #[template_callback]
    fn add_to_counter(&self) {
        let lock = self.lock_state();
        if !lock {
            let n = self.counter() + 1;
            self.set_counter(n)
        }
    }

    /// Callback handler for gtk::Button lock.
    #[template_callback]
    fn lock_toggle(&self, button: &gtk::Button) {
        let n = self.lock_state();
        match n {
            true => {
                self.set_lock_state(false);
                button.set_label("unlock");
            }
            false => {
                self.set_lock_state(true);
                button.set_label("lock");
            }
        }

    }

    /// Callback handler for gtk::Entry text.
    #[template_callback]
    fn text_entry(&self, _entry: &gtk::Entry) {
    }
}
