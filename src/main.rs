mod window;

use gtk::{glib, prelude::*};
use window::TallyWindow;

const APP_ID: &str = "org.tally-rs.Tally";

fn main() -> glib::ExitCode {
    let application = gtk::Application::builder()
        .application_id(APP_ID)
        .build();

    application.connect_activate(|app| {
        let win = TallyWindow::new(app);
        win.set_lock_state(false);
        win.present();
    });

    application.run()
}
